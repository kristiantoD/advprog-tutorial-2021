package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

public class HighSpiritDefenseSpell extends HighSpiritSpell {
    // TODO: Complete Me

    @Override
    public String spellName() {
        return spirit.getRace() + ":Defense";
    }
}
